#LIS 4381 Mobile Application Development and Management Fall 2015 P1

#Alexander Garcia

## Bitbucket Repo Link

[Bitbucket Repo Link](https://bitbucket.org/amg13g/lis4381_fall15 "Bitbucket Repo Link")

## My Website Link

[alexandermiguelgarcia.com](http://alexandermiguelgarcia.com/ "alexandermiguelgarcia.com")

##Assignment Requirements:

1. Download and review given files.
2. Add client-side form validation for various form fields
3. Upload file to bitbucket and to your webhost
4. Links to the Repo and your website