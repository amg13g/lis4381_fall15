<?php
  //get all petstores
function get_all_petstores()
{
	global $db;
	
	$query = "SELECT * FROM store ORDER BY sto_id";
	try{
		$statement = $db->prepare($query);
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	}catch(PDOException $e){
		$error = $e->getMessage();
		display_db_error($error);
	}
}

  //get individual petstore
function get_petstore($sto_id_v) 
{
	global $db;
	
	$query = "SELECT * FROM store WHERE sto_id = :sto_id_p";
	try{
		$statement = $db->prepare($query);
		$statement->bindParam(':sto_id_p', $sto_id_v);
		$statement->execute();
		$result = $statement->fetchAll();
		$statement->closeCursor();
		return $result;
	}catch(PDOException $e){
		$error = $e->getMessage();
		display_db_error($error);
	}
}

  //add petstore
function add_petstore($sto_name_v, $sto_street_v, $sto_city_v, $sto_state_v, $sto_zip_v, $sto_phone_v, $sto_email_v, $sto_url_v, $sto_ytd_sales_v, $sto_notes_v) 
{
	global $db;
	
	$query = 
	"INSERT INTO store
	(sto_name, sto_street, sto_city, sto_state, sto_zip, sto_phone, sto_email, sto_url, sto_ytd_sales, sto_notes)
	VALUES
	(:sto_name_v, :sto_street_v, :sto_city_v, :sto_state_v, :sto_zip_v, :sto_phone_v, :sto_email_v, :sto_url_v, :sto_ytd_sales_v, :sto_notes_v)";

	try{
		$statement = $db->prepare($query);
		$statement->bindParam(':sto_name_v', $sto_name_v);
		$statement->bindParam(':sto_street_v', $sto_street_v);
		$statement->bindParam(':sto_city_v', $sto_city_v);
		$statement->bindParam(':sto_state_v', $sto_state_v);
		$statement->bindParam(':sto_zip_v', $sto_zip_v);
		$statement->bindParam(':sto_phone_v', $sto_phone_v);
		$statement->bindParam(':sto_email_v', $sto_email_v);
		$statement->bindParam(':sto_url_v', $sto_url_v);
		$statement->bindParam(':sto_ytd_sales_v', $sto_ytd_sales_v);
		$statement->bindParam(':sto_notes_v', $sto_notes_v);
		$statement->execute();
		$statement->closeCursor();

		$last_auti_increment_id = $db->lastInsertId();

	}catch(PDOException $e){

		$error = $e->getmessage();
		display_db_error($error);
	}
}

  //edit petstore
function edit_petstore($sto_id_v, $sto_name_v, $sto_street_v, $sto_city_v, $sto_state_v, $sto_zip_v, $sto_phone_v, $sto_email_v, $sto_url_v, $sto_ytd_sales_v, $sto_notes_v) 
{
	global $db;	
	
	$query = 
	"UPDATE store 
	SET
	 sto_name = :sto_name_p,
	 sto_street = :sto_street_p,
	 sto_city = :sto_city_p,
	 sto_state = :sto_state_p,
	 sto_zip = :sto_zip_p,
	 sto_phone = :sto_phone_p,
	 sto_email = :sto_email_p,
	 sto_url = :sto_url_p,
	 sto_ytd_sales = :sto_ytd_sales_p,
	 sto_notes = :sto_notes_p 
	WHERE sto_id = :sto_id_p";
	try{
		$statement = $db->prepare($query);
		$statement->bindParam(':sto_id_p', $sto_id_v);
		$statement->bindParam(':sto_name_p', $sto_name_v);
		$statement->bindParam(':sto_street_p', $sto_street_v);
		$statement->bindParam(':sto_city_p', $sto_city_v);
		$statement->bindParam(':sto_state_p', $sto_state_v);
		$statement->bindParam(':sto_zip_p', $sto_zip_v);
		$statement->bindParam(':sto_phone_p', $sto_phone_v);
		$statement->bindParam(':sto_email_p', $sto_email_v);
		$statement->bindParam(':sto_url_p', $sto_url_v);
		$statement->bindParam(':sto_ytd_sales_p', $sto_ytd_sales_v);
		$statement->bindParam(':sto_notes_p', $sto_notes_v);
		$row_count = $statement->execute();
		$statement->closeCursor();

	}catch(PDOException $e){

		$error = $e->getmessage();
		display_db_error($error);

	}
}

  //delete petstore
function delete_petstore($sto_id_v) 
{
	global $db;	
	
	$query = 
	"DELETE FROM store 
	WHERE sto_id = :sto_id_p";

	try{
		$statement = $db->prepare($query);
		$statement->bindParam(':sto_id_p', $sto_id_v);
		$row_count = $statement->execute();
		$statement->closeCursor();
		
		header('location: index.php');
	}
	catch(PDOException $e){
		$error = $e->getMessage();
		display_db_error($error);
	}
}
?>
