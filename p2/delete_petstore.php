<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

//exit(print_r($_POST));

$sto_id_v = $_POST['sto_id'];

if(empty($sto_id_v)){
	$error = "Invalid data. Check field and try again.";
	include('../lis4381_global/error.php');
}else{

	require_once('../lis4381_global/connection.php');
	require_once('../lis4381_global/functions.php');

	delete_petstore($sto_id_v);
	
	header('location: index.php');
}
?>