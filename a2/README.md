#LIS 4381 Mobile Application Development and Management Fall 2015 

#Alexander Garcia

## Bitbucket Repo Link

[Bitbucket Repo Link](https://bitbucket.org/amg13g/lis4381_fall15 "Bitbucket Repo Link")

## My Website Link

[alexandermiguelgarcia.com](http://alexandermiguelgarcia.com/ "alexandermiguelgarcia.com")

##Assignment Requirements:

1. Create remote lis4381 repo in your Bitbucket account
2. cd to local repos subdirectory: Example: cd c:/webdev/repos
3. Clone assignment starter files: git clone https://bitbucket.org/mjowett/lis4381_starter_files lis4381 (clones lis4381_starter_files directory into lis4381 local directory)
4. Review subdirectories and files
5. Open index.php and review code:
>a. Correct link paths: modify necessary files, make *all* links relative paths
>
>b. Change course link name, and create process.php (see below)
>
>c. Use git to push *all* files and changes in your local lis4381 repo to your remote lis4381 Bitbucket repo
>
>d. Provide me read-only access to your lis4381 Bitbucket repo
>
6. Upload lis4381 files to your Web host