#LIS 4381 Mobile Application Development and Management Fall 2015 A3

#Alexander Garcia

## Bitbucket Repo Link

[Bitbucket Repo Link](https://bitbucket.org/amg13g/lis4381_fall15 "Bitbucket Repo Link")

## My Website Link

[alexandermiguelgarcia.com](http://alexandermiguelgarcia.com/ "alexandermiguelgarcia.com")

##Assignment Requirements:

1. Create an Entity and Relationship Diagram (ERD)
2. Include data of at least 10 records for each table
3. Provide Bitbucket read-only access to lis4381a3 repo (Language SQL), include all files:
>a3.mwb
>
>a3.sql
>
>a3.png (a3.mwb file exported as a png)
>
>README.md (Must Display a3.png ERD)
>
4. Links to the Repo and your website

###ERD Image:

![ERD Image](https://www.bitbucket.org/amg13g/lis4381_fall15/raw/master/a3/images/a3.png "ERD Image") 