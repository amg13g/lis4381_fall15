<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
//exit(print_r($_POST));

//get item data
$sto_name_v = $_POST['name'];
$sto_street_v = $_POST['street'];
$sto_city_v = $_POST['city'];
$sto_state_v = $_POST['state'];
$sto_zip_v = $_POST['zip'];
$sto_phone_v = $_POST['phone'];
$sto_email_v = $_POST['email'];
$sto_url_v = $_POST['url'];
$sto_ytd_sales_v = $_POST['ytdsales'];
$sto_notes_v = $_POST['notes'];

$pattern='/^[a-zA-Z\s]+$/';
$valid_city = preg_match($pattern, $sto_city_v);

$pattern='/^[a-zA-Z]{2,2}+$/';
$valid_state = preg_match($pattern, $sto_state_v);

$pattern='/^\d{5,9}+$/';
$valid_zip = preg_match($pattern, $sto_zip_v);

$pattern='/^\d{10}+$/';
$valid_phone = preg_match($pattern, $sto_phone_v);

$pattern='/^\d{1,8}(?:\.\d{0,2})?$/';
$valid_ytd_sales = preg_match($pattern, $sto_ytd_sales_v);

//exit(print_r(get_defined_vars()));

if(
	empty($sto_name_v)||
	empty($sto_street_v)||
	empty($sto_city_v)||
	empty($sto_state_v)||
	empty($sto_zip_v)||
	empty($sto_phone_v)||
	empty($sto_email_v)||
	empty($sto_url_v)||
	empty($sto_ytd_sales_v)
){
	$error = "All fields require data, except <b>Notes</b>. Check all fields and try again.";
	include('../lis4381_global/error.php');
}else if (!is_numeric($sto_ytd_sales_v) || $sto_ytd_sales_v <= 0){
	$error = 'YTD Sales can only contain numbers (other than a decimal point); and must be greater than or equal to zero.';
	include('../lis4381_global/error.php');
}else if ($valid_city === false){
	echo 'Error in pattern!';
}else if ($valid_city === 0){
	$error = 'City can only contain letters.';
	include('../lis4381_global/error.php');
}else if ($valid_state === false){
	echo 'Error in pattern!';
}else if ($valid_state === 0){
	$error = 'State must contain two letters.';
	include('../lis4381_global/error.php');
}else if ($valid_zip === false){
	echo 'Error in pattern!';
}else if ($valid_zip === 0){
	$error = 'Zip must contain 5 - 9 digits and no other characters';
	include('../lis4381_global/error.php');
}else if ($valid_phone === false){
	echo 'Error in pattern!';
}else if ($valid_phone === 0){
	$error = 'Phone must contain 10 digits and no other characters';
	include('../lis4381_global/error.php');
}else if ($valid_ytd_sales === false){
	echo 'Error in pattern!';
}else if ($valid_ytd_sales === 0){
	$error = 'YTD_Sales must contain no more than 10 digits, including a decimal point.';
	include('../lis4381_global/error.php');
}else{
	require_once('../lis4381_global/connection.php');
	

$query = 
"INSERT INTO store
(sto_name, sto_street, sto_city, sto_state, sto_zip, sto_phone, sto_email, sto_url, sto_ytd_sales, sto_notes)
VALUES
(:sto_name_v, :sto_street_v, :sto_city_v, :sto_state_v, :sto_zip_v, :sto_phone_v, :sto_email_v, :sto_url_v, :sto_ytd_sales_v, :sto_notes_v)";

try{
	$statement = $db->prepare($query);
	$statement->bindParam(':sto_name_v', $sto_name_v);
	$statement->bindParam(':sto_street_v', $sto_street_v);
	$statement->bindParam(':sto_city_v', $sto_city_v);
	$statement->bindParam(':sto_state_v', $sto_state_v);
	$statement->bindParam(':sto_zip_v', $sto_zip_v);
	$statement->bindParam(':sto_phone_v', $sto_phone_v);
	$statement->bindParam(':sto_email_v', $sto_email_v);
	$statement->bindParam(':sto_url_v', $sto_url_v);
	$statement->bindParam(':sto_ytd_sales_v', $sto_ytd_sales_v);
	$statement->bindParam(':sto_notes_v', $sto_notes_v);
	$statement->execute();
	$statement->closeCursor();

	$last_auti_increment_id = $db->lastInsertId();

}catch(PDOException $e){

	$error = $e->getmessage();
	echo $error;

}

//include('index.php'); //forwarding is faster, one trip to server
header('Location: index.php'); //sometimes, redirecting is needed (two trips to server)
}
?>
