<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

//exit(print_r($_POST));

$sto_id_v = $_POST['sto_id'];

require_once('../lis4381_global/connection.php');

$query = 
"DELETE FROM store 
WHERE sto_id = :sto_id_p";

try{
	$statement = $db->prepare($query);
	$statement->bindParam(':sto_id_p', $sto_id_v);
	$row_count = $statement->execute();
	$statement->closeCursor();
	
	header('location: index.php');
}
catch(PDOException $e){
	$error = $e->getMessage();
	echo $error;
}
?>