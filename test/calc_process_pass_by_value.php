<?php

//show errors: at least lines 1 and 4
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(_FILE_).'/error_log.txt');
error_reporting(E_ALL);

//use for initial test of form inputs -- also, demo no htmlspecialchars()
//exit(print_r($_POST));

$num1 = $_POST['num1'];
$num2 = $_POST['num2'];
$calcs = $_POST['calcs'];

if ($num1 == ""){
	$num1 = 0;
}
if ($num2 == ""){
	$num2 = 0;
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Alexander Garcia">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment Cal</title>		

<!-- Bootstrap core CSS -->
<!-- Latest compiled and minified CSS -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->
<link href="../css/mit_css.min.css" rel="stylesheet">

<!-- Optional theme -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css"> -->
<link href="../css/mit_css_theme.min.css" rel="stylesheet">

<!-- Custom styles with this template -->
<link href="../css/starter-template.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
  </head>

  <body>

  <?php include_once("../lis4381_global/nav.php"); ?>
	
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<h1>Simple Calculator</h1>
					<p> Preforms Addition, Subtraction, Multiplication, Division, and Exponentiation</p>
				</div>

<br>
<h2>
	<?php
		if ($calcs == 'addition'){
			echo "Addition";
		}else
		if ($calcs == 'subtraction'){
			echo "Subtraction";
		}else
		if ($calcs == 'multiplication'){
			echo "Multiplication";
		}else
		if ($calcs == 'division'){
			echo "Division";
		}else
		if ($calcs == 'exponentiation'){
			echo "Exponentiation";
		}else 
			echo "Error! No method selected";
		
	?>
</h2>
<br>
	<?php 
		echo $num1;
		
		if ($calcs == 'addition'){
			echo " + ";
		}else
		if ($calcs == 'subtraction'){
			echo " - ";
		}else
		if ($calcs == 'multiplication'){
			echo " * ";
		}else
		if ($calcs == 'division'){
			echo " / ";
		}else
		if ($calcs == 'exponentiation'){
			echo " raised to the power of ";
		}else
			echo " ??? ";
		
		echo $num2;
		echo " = ";
		
		if ($calcs == 'addition'){
			echo $num1 + $num2;
		}else
		if ($calcs == 'subtraction'){
			echo $num1 - $num2;
		}else
		if ($calcs == 'multiplication'){
			echo $num1 * $num2;
		}else
		if ($calcs == 'division'){
			if($num2 != 0){
			echo $num1 / $num2;
			}else
			echo "Who Knows? <br> You cannot Divide by 0!";
		}else
		if ($calcs == 'exponentiation'){
			echo pow($num1, $num2);
		}else
			echo " ??? ";
	?>
<br><br>
				
  <?php include_once "../lis4381_global/footer.php"; ?>

		</div> <!-- starter-template -->
    </div> <!-- end container -->

    <!-- Bootstrap core JavaScript: jQuery necessary for Bootstrap's JavaScript plugins
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
		<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>

		<!-- Latest compiled and minified JavaScript -->
		<script type="text/javascript" src="js/bootstrap.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
