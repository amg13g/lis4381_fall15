<?php 
	class Employee_class extends Person_class{
	private $ssn;
	private $gender;
	
	public function __construct($passedFname = 'John', $passedLname = 'Doe', $passedAge = '0', $passedSsn = '000-00-0000', $passedGender = 'M'){
		$this->ssn = $passedSsn;
		$this->gender = $passedGender;

		parent::__construct($passedFname, $passedLname, $passedAge);
		
		echo("Creating <strong>". parent::GetFname() . " ". parent::GetLname() ."</strong> employee object.<br />");

	}

	public function SetSsn($passedSsn){
		$this->fname = $passedSsn;
	}

	public function SetGender($passedGender){
		$this->lname = $passedGender;
	}

	public function GetSsn(){
		return $this->ssn;
	}

	public function GetGender(){
		return $this->gender;
	}
	
	function __destruct(){
		parent::__destruct();
		echo("Destroying <strong>". parent::GetFname() . " ". parent::GetLname() ."</strong> employee object.<br />");
	}
}
?>