<?php 
	class Person_class{
	private $fname;
	private $lname;
	private $Age;
	
	public function __construct($passedFname = 'John', $passedLname = 'Doe', $passedAge = '0'){
		$this->fname = $passedFname;
		$this->lname = $passedLname;
		$this->age = $passedAge;
		
		echo("Creating <strong>".$this->fname." ".$this->lname."</strong> person object.<br />");

	}

	public function SetFname($passedFname){
		$this->fname = $passedFname;
	}

	public function SetLname($passedLname){
		$this->lname = $passedLname;
	}
	
	public function SetAge($passedAge){
		$this->age = $passedAge;
	}

	public function GetFname(){
		return $this->fname;
	}

	public function GetLname(){
		return $this->lname;
	}
	
	public function GetAge(){
		return $this->age;
	}
	
	function __destruct(){
		echo("Destroying <strong>".$this->fname." ".$this->lname."</strong> person object.<br />");
	}
}
?>