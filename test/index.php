<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Alexander Garcia">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - AssignmentXX</title>		

<!-- Bootstrap core CSS -->
<!-- Latest compiled and minified CSS -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->
<link href="../css/mit_css.min.css" rel="stylesheet">

<!-- Optional theme -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css"> -->
<link href="../css/mit_css_theme.min.css" rel="stylesheet">

<!-- Custom styles with this template -->
<link href="../css/starter-template.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

  </head>

  <body>

  <?php include_once("../lis4381_global/nav.php"); ?>
	
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("../lis4381_global/header.php"); ?>	
				</div>

<br><br>
<p> This page was intentionally left (mostly) blank for future use.</p>
<br><br>
<p>Links to test projects:</p>
<br>
<a href="calc_index.php">Calculator</a><br>
<a href="calc_index_pass_by_value.php">Calculator, values passed by value</a><br>
<a href="calc_index_pass_by_reference.php">Calculator, values passed by reference</a></br>
<a href="person.php">Simple person class</a></br>
<a href="employee.php">Employee class inheritence</a></br>
<a href="rss.php">RSS Feed</a></br>




<br><br>

				
  <?php include_once "../lis4381_global/footer.php"; ?>

		</div> <!-- starter-template -->
    </div> <!-- end container -->

    <!-- Bootstrap core JavaScript: jQuery necessary for Bootstrap's JavaScript plugins
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
		<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>

		<!-- Latest compiled and minified JavaScript -->
		<script type="text/javascript" src="js/bootstrap.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
