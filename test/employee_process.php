<?php
	require_once("classes/person_class.php");
	require_once("classes/employee_class.php");

	
	//get user input
	$passedFname = $_POST['fname'];
	$passedLname = $_POST['lname'];
	$passedAge = $_POST['age'];
	$passedSsn = $_POST['ssn'];
	$passedGender = $_POST['gender'];
	
	//exit(print_r($_POST));
	
	$employee1 = new Employee_class();
	$employee2 = new Employee_class($passedFname, $passedLname, $passedAge, $passedSsn, $passedGender);
	
	//exit(print_r($person2->GetFname()));

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Alexander Garcia">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment: Employee Inheritence Skillset</title>		

<!-- Bootstrap core CSS -->
<!-- Latest compiled and minified CSS -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->
<link href="../css/mit_css.min.css" rel="stylesheet">

<!-- Optional theme -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css"> -->
<link href="../css/mit_css_theme.min.css" rel="stylesheet">

<!-- Custom styles with this template -->
<link href="../css/starter-template.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

  </head>

  <body>

  <?php include_once("../lis4381_global/nav.php"); ?>
	
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<h1>Employee Inheritence Class</h1>
					<p>Displays first and last names, age, social, and gender.</p>
				</div>

<br><br>
<div class="table-responsive">
	<table id="myTable" class="table table-striped table-condensed" >
		<thread>
			<tr>
				<th>FName</th>
				<th>LName</th>
				<th>Age</th>
				<th>Ssn</th>
				<th>Gender</th>
			</tr>
		</thread>
		<tr>
			<td><?php echo $employee1->GetFname(); ?></td>
			<td><?php echo $employee1->GetLname(); ?></td>
			<td><?php echo $employee1->GetAge(); ?></td>
			<td><?php echo $employee1->GetSsn(); ?></td>
			<td><?php echo $employee1->GetGender(); ?></td>

		</tr>
		<tr>
			<td><?php echo $employee2->GetFname(); ?></td>
			<td><?php echo $employee2->GetLname(); ?></td>
			<td><?php echo $employee2->GetAge(); ?></td>
			<td><?php echo $employee2->GetSsn(); ?></td>
			<td><?php echo $employee2->GetGender(); ?></td>
		</tr>
	</table>
</div>
<br><br>
				
  <?php include_once "../lis4381_global/footer.php"; ?>

		</div> <!-- starter-template -->
    </div> <!-- end container -->

    <!-- Bootstrap core JavaScript: jQuery necessary for Bootstrap's JavaScript plugins
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
		<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>

		<!-- Latest compiled and minified JavaScript -->
		<script type="text/javascript" src="js/bootstrap.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
