<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Alexander Garcia">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment: Employee Inheritence Skillset</title>		

<!-- Bootstrap core CSS -->
<!-- Latest compiled and minified CSS -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->
<link href="../css/mit_css.min.css" rel="stylesheet">

<!-- Optional theme -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css"> -->
<link href="../css/mit_css_theme.min.css" rel="stylesheet">

<!-- Custom styles with this template -->
<link href="../css/starter-template.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

  </head>

  <body>

  <?php include_once("../lis4381_global/nav.php"); ?>
	
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<h1>Employee Inheritence Skillset</h1>
					<p>Displays first and last names, age, social, and gender.</p>
				</div>

<br><br>
<form class="form-horizontal" role="form" method="post" action="employee_process.php">

					<div class="form-group">
						<label class="control-label col-sm-2" for="age">First Name:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="fname" id="fname" placeholder="Enter your first name">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2" for="age">Last Name:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="lname" id="lname" placeholder="Enter your last name">
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-sm-2" for="age">Age:</label>
						<div class="col-sm-10">
							<select class="form-control" name="age" id="age">
								<?php
									for($i=18;$i<66;$i++){
										echo "<option value=$i>$i</option>";
									}
								?>
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-sm-2" for="age">Social Security number:</label>
						<div class="col-sm-10">
						
							<input type="text" class="form-control" name="ssn" id="ssn" placeholder="Enter your social security number">
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-sm-2" for="age">Gender:</label>
						<div class="col-sm-10">
							<input type="radio" class="radio-inline" name="gender" id="gender" value="M">Male
							<input type="radio" class="radio-inline" name="gender" id="gender" value="F">Female
						</div>
					</div>
					
					<div class="form-group">        
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-default">Submit</button>
						</div>
					</div>
</form>
<br><br>
				
  <?php include_once "../lis4381_global/footer.php"; ?>

		</div> <!-- starter-template -->
    </div> <!-- end container -->

    <!-- Bootstrap core JavaScript: jQuery necessary for Bootstrap's JavaScript plugins
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
		<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>

		<!-- Latest compiled and minified JavaScript -->
		<script type="text/javascript" src="js/bootstrap.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
